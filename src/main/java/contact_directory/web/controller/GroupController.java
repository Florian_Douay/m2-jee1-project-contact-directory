package contact_directory.web.controller;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import contact_directory.business.GroupManager;
import contact_directory.business.PersonManager;
import contact_directory.dao.Group;
import contact_directory.dao.Person;
import contact_directory.dao.exception.DAOException;

@Controller()
@RequestMapping("/groups")
public class GroupController {

    @Autowired
    private GroupManager groupManager;
    @Autowired
    private PersonManager personManager;

    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView listGroups() throws DAOException {
        logger.info("List of groups");
        Collection<Group> groups = null;

        groups = groupManager.findAllGroups();

        return new ModelAndView("groupsList", "groups", groups);
    }
    
    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public ModelAndView showGroup(@RequestParam(value = "id") Long value) throws DAOException {
        logger.info("show group");
        Group group = null;
        Collection<Person> persons = null;

        group = groupManager.findGroup(value);
        persons = personManager.findAllPersons(value);

        return new ModelAndView("group").addObject("group", group).addObject("persons", persons);
    }
    
}
