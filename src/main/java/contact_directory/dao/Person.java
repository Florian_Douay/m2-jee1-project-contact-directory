package contact_directory.dao;

import java.sql.Date;

public class Person {

    private Long id;
    private String lastName;
    private String firstName;
    private String email;
    private String website;
    private Date birthdate;
    private String password;
    private Long idGroup;

    public Person() {}

    public Person(String lastName, String firstName, String email, String website, Date birthdate, String password, Long idGroup) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.website = website;
        this.birthdate = birthdate;
        this.password = password;
        this.idGroup = idGroup;
    }

    public Long getId() {
        return id;
    }
    protected void setId(Long id) {
        this.id = id;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }
    public Date getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
    public String getPassword() {
        return password;
    }
    /**
     * Passwords are stored in plaintext because security is out of the scope of
     * this project. We are well aware that real passwords must be hashed and
     * salted. Hashing with a key derivation function is even better.
     */

    public void setPassword(String password) {
        this.password = password;
    }
    public Long getGroupId() {
        return idGroup;
    }
    public void setGroupId(Long idGroup) {
        this.idGroup = idGroup;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((birthdate == null) ? 0 : birthdate.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((idGroup == null) ? 0 : idGroup.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((website == null) ? 0 : website.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Person)) return false;
        Person other = (Person) obj;
        if (birthdate == null) {
            if (other.birthdate != null) return false;
        } else if (!birthdate.equals(other.birthdate)) return false;
        if (email == null) {
            if (other.email != null) return false;
        } else if (!email.equals(other.email)) return false;
        if (firstName == null) {
            if (other.firstName != null) return false;
        } else if (!firstName.equals(other.firstName)) return false;
        if (id == null) {
            if (other.id != null) return false;
        } else if (!id.equals(other.id)) return false;
        if (idGroup == null) {
            if (other.idGroup != null) return false;
        } else if (!idGroup.equals(other.idGroup)) return false;
        if (lastName == null) {
            if (other.lastName != null) return false;
        } else if (!lastName.equals(other.lastName)) return false;
        if (password == null) {
            if (other.password != null) return false;
        } else if (!password.equals(other.password)) return false;
        if (website == null) {
            if (other.website != null) return false;
        } else if (!website.equals(other.website)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", email=" + email
                + ", website=" + website + ", birthdate=" + birthdate + ", password=" + password + ", idGroup="
                + idGroup + "]";
    }
    
}
