package contact_directory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contact_directory.dao.exception.DAOException;
import contact_directory.test.utils.JdbcToolsTestHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/spring-test.xml")
public class JdbcToolsTest {

    @Autowired
    private JdbcToolsTestHelper jdbcToolTestHelper;

    @Test(expected=DAOException.class)
    public void testWrongUrl() throws DAOException, SQLException {
        jdbcToolTestHelper.getDataSource().setUrl("random");
        try(Connection conn = jdbcToolTestHelper.getConnection()) {}
    }

    @Test(expected=DAOException.class)
    public void testWrongUser() throws DAOException, SQLException {
        jdbcToolTestHelper.getDataSource().setUsername("random");
        try(Connection conn = jdbcToolTestHelper.getConnection()) {}
    }

    @Test(expected=DAOException.class)
    public void testWrongPswd() throws DAOException, SQLException {
        jdbcToolTestHelper.getDataSource().setPassword("random");
        try(Connection conn = jdbcToolTestHelper.getConnection()) {}
    }

    @Test
    public void testCorrectConnection() throws DAOException, SQLException {
        try(Connection conn = jdbcToolTestHelper.getConnection()) {
            assertFalse(conn.isClosed());
        }
    }
    
    @Test
    public void testCloseConnection1() throws DAOException, SQLException {
        Connection conn = jdbcToolTestHelper.getConnection();
        assertFalse(conn.isClosed());
        jdbcToolTestHelper.close();
        assertFalse(conn.isClosed());
        conn.close();
    }
    
    @Test(expected=DAOException.class)
    public void testCloseConnection2() throws DAOException, SQLException {
        
        try {
            Connection conn = jdbcToolTestHelper.getConnection();
            assertFalse(conn.isClosed());
            jdbcToolTestHelper.close();
            assertFalse(conn.isClosed());
        }
        catch (DAOException e) {
            fail("Unexcepted dao exception at this point");
        }
        
        jdbcToolTestHelper.getConnection();
    }

}
