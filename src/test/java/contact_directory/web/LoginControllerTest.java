package contact_directory.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import contact_directory.dao.Person;
import contact_directory.dao.PersonDaoJdbc;
import contact_directory.test.utils.DbSetupper;
import contact_directory.test.utils.DbUnitDataSet;
import contact_directory.web.bean.UserInSession;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = "/spring-test.xml")
public class LoginControllerTest {

    @Autowired
    DbSetupper dbSetupper;

    @Autowired
    @Qualifier("personDaoJdbc")
    private PersonDaoJdbc dao;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        dbSetupper.setupDbForTests(DbUnitDataSet.ONE_USER_ONE_GROUP);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testLoginPage() throws Exception {
        mockMvc.perform(get("/login"))
        .andExpect(view().name("login"))
        .andExpect(status().isOk());
    }

    @Test
    public void testLogin() throws Exception {
        HttpSession session = mockMvc.perform(post("/login")
                .param("email", "email@example.org")
                .param("password", "password")
                )
                .andExpect(redirectedUrl("/contact_directory/search"))
                .andReturn().getRequest().getSession();
        UserInSession userInSession = (UserInSession) session.getAttribute("scopedTarget.userInSession");
        assertNotNull(userInSession);
        long userId = userInSession.getId();
        Person person = dao.findPerson(userId); // DAOException here means that user wasn't found
        assertEquals(person.getId().longValue(), userId);
        assertEquals(person.getGroupId().longValue(), userInSession.getGroupId());
    }

    @Test
    public void testLoginWrongPassword() throws Exception {
        mockMvc.perform(post("/login")
                .param("email", "email@example.org")
                .param("password", "WRONG password")
                )
                .andExpect(status().isForbidden())
                .andExpect(view().name("login"));
        // TODO assert error message
    }

    @Test
    public void testLogoutWhenNotLoggedIn() throws Exception {
        mockMvc.perform(post("/logout"))
                .andExpect(redirectedUrl("/contact_directory/search"));
    }

    @Test
    public void testLogoutWhenLoggedIn() throws Exception {
        // We put something in session, anything to ensure that the whole
        // session is deleted
        HashMap<String, Object> sessionattr = new HashMap<>();
        sessionattr.put("foo", "bar");

        HttpSession session = mockMvc
                .perform(post("/logout").sessionAttrs(sessionattr))
                .andExpect(redirectedUrl("/contact_directory/search"))
                .andReturn().getRequest().getSession();
        // check that what we put in session in gone
        assertNull(session.getAttribute("foo"));
    }
}
