package contact_directory.test.utils;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import contact_directory.dao.Person;

@Service("personTestHelper")
@Scope("prototype")
public class PersonTestHelper extends Person {

    @Override
    public void setId(Long id) {
        super.setId(id);
    }
    
}
