package contact_directory.test.utils;

import java.io.FileInputStream;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import contact_directory.dao.exception.DAOException;

@Component
public class DbSetupper {

    @Autowired
    private JdbcDatabaseConfigBean dbConfig;
    @Autowired
    private DataBaseTestTools dataBaseTestTools;

    private IDatabaseTester databaseTester;
    private static boolean isInitialSetupDone = false;

    public void setupDbForTests(DbUnitDataSet dbUnitDataSet) throws Exception {
        /*
         * It's a dirty way to do a setup once without using @BeforeClass which must be static.
         * Because we need non static members for it (dao to drop/create tables)
         */
        if(!isInitialSetupDone) {
            initialSetUp();
            isInitialSetupDone = true;
        }
        databaseTester = new JdbcDatabaseTester(dbConfig.getDriverClass(),
        dbConfig.getConnectionUrl(), dbConfig.getUsername(), dbConfig.getPassword());
        FileInputStream fileInputStream = new FileInputStream(dbUnitDataSet.getFilePath());
        IDataSet dataSet = new FlatXmlDataSetBuilder().build(fileInputStream);
        databaseTester.setDataSet(dataSet);
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.onSetup();
        fileInputStream.close();
    }

    private void initialSetUp() throws DAOException {
        dataBaseTestTools.dropTables();
        dataBaseTestTools.createTables();
    }
}