<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<!-- Our custom tags -->
<%@ taglib prefix="cus" tagdir="/WEB-INF/tags" %>

<c:url var="searchPage" value="/contact_directory/search" />
<c:url var="groupsListPage" value="/contact_directory/groups/list" />
<c:url var="groupPage" value="/contact_directory/groups/group" />
<c:url var="personPage" value="/contact_directory/persons/person" />
<c:url var="editPersonPage" value="/contact_directory/persons/edit" />
<c:url var="logoutPage" value="/contact_directory/logout" />
<c:url var="loginPage" value="/contact_directory/login" />