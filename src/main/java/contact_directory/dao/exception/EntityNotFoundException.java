package contact_directory.dao.exception;

import java.sql.SQLException;

@SuppressWarnings("serial")
public class EntityNotFoundException extends DAOException {

    public EntityNotFoundException(String message, SQLException cause) {
        super(message, cause);
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException() {
        super();
    }

}
