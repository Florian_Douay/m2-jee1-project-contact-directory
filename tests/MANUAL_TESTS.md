The only area that should be manually tested are the web pages.

It's very costly to test for regressions on the layout and CSS.


## Redirections
### Go to / for be redirected to search page

## Error pages
### When trying to edit someone else
- Login
- Go to your profile
- Click on edit
- Check that your are on the edit form
- Change the id in the URL
- Go to that URL
- You should see the error page

### Go to /non-existing see the error page
