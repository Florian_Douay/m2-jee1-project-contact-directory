package contact_directory.web.controller;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import contact_directory.business.GroupManager;
import contact_directory.business.PersonManager;
import contact_directory.dao.Group;
import contact_directory.dao.Person;
import contact_directory.dao.PersonColumns;
import contact_directory.dao.exception.DAOException;
import contact_directory.web.bean.SearchBean;
import contact_directory.web.validators.SearchValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;

@Controller()
public class SearchController {

    @Autowired
    private GroupManager groupManager;
    @Autowired
    private PersonManager personManager;
    @Autowired
    private SearchValidator validator;
    
    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView show() {
        logger.info("Returning search view");
        return new ModelAndView("search");
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView search(@ModelAttribute SearchBean sb, BindingResult result) throws DAOException {

        validator.validate(sb, result);
        if (result.hasErrors()) {
            return new ModelAndView("search");
        }
        
        //search group
        if(sb.getType().equals("group")) {
            Collection<Group> groups = null;
            //TODO use group criteria
            groups = groupManager.searchForGroups(sb.getToken());
            
            if(groups.size() == 0) {
                logger.info("Returning search view with zero groups as results");
                return new ModelAndView("search").addObject("emptyResults", true);
            }
            
            logger.info("Returning search view with " + groups.size() + " groups as results");
            return new ModelAndView("search").addObject("groups", groups);
        }

        //search person
        Collection<Person> persons = null;
        persons = personManager.searchForPersons(sb.getToken(), PersonColumns.valueOf(sb.getPersonCriteria()));
        
        if(persons.size() == 0) {
            logger.info("Returning search view with zero person as results");
            return new ModelAndView("search").addObject("emptyResults", true);
        }

        logger.info("Returning search view with " + persons.size() + " persons as results");
        return new ModelAndView("search").addObject("persons", persons);
    }
    
    @ModelAttribute
    public SearchBean newSearchBean(@RequestParam(value = "token", required = false) String token,
                                    @RequestParam(value = "type", required = false) String type,
                                    @RequestParam(value = "groupCriteria", required = false) String groupCriteria,
                                    @RequestParam(value = "personCriteria", required = false) String personCriteria) {

        SearchBean sb = new SearchBean();
        sb.setToken(token);
        sb.setType(type);
        sb.setGroupCriteria(groupCriteria);
        sb.setPersonCriteria(personCriteria);
        logger.info("new searchBean = " + sb);
        return sb;
    }
    
    @ModelAttribute("searchType")
    public static Map<String, String> searchTypes() {
        Map<String, String> types = new LinkedHashMap<>();
        types.put("group", "Group");
        types.put("person", "Person");
        return types;
    }
    
    @ModelAttribute("searchGroupCriterias")
    public static Map<String, String> searchGroupCriteria() {
        Map<String, String> types = new LinkedHashMap<>();
        types.put("name", "Name");
        return types;
    
    }
    @ModelAttribute("searchPersonCriterias")
    public static Map<String, String> searchPersonCriteria() {
        Map<String, String> types = new LinkedHashMap<>();
        PersonColumns[] values = PersonColumns.values();
        for(PersonColumns column : values) {
            if(column == PersonColumns.ID || column == PersonColumns.PASSWORD || column == PersonColumns.GROUP_ID) continue;
            types.put(column.name(), column.toString());
        }
        return types;
    }
    
}
