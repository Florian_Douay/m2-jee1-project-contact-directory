package contact_directory.web.validators;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import contact_directory.dao.PersonColumns;
import contact_directory.web.bean.SearchBean;

@Service
public class SearchValidator implements Validator {

    protected final Log logger = LogFactory.getLog(getClass());
    
    @Override
    public boolean supports(Class<?> clazz) {
        return SearchBean.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SearchBean sb = (SearchBean) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "token", "sb.token", "Token is required.");
        
        if(sb.getType().equals("group")) {
            if(sb.getGroupCriteria().equals("name") == false) {
                errors.rejectValue("groupCriteria", "sb.group.criteria.unknown", "Unknown criteria for group");
            }
        }
        else if(sb.getType().equals("person")) {
            PersonColumns column;
            try {
                column = PersonColumns.valueOf(sb.getPersonCriteria());
                if(column == PersonColumns.ID) {
                    errors.rejectValue("personCriteria", "sb.person.criteria.unknown", "Unknown criteria for person");
                }
                else if(column == PersonColumns.PASSWORD) {
                    errors.rejectValue("personCriteria", "sb.person.criteria.unknown", "Unknown criteria for person");
                }
                else if(column == PersonColumns.GROUP_ID) {
                    errors.rejectValue("personCriteria", "sb.person.criteria.unknown", "Unknown criteria for person");
                }
                
                if(column == PersonColumns.BIRTH_DATE) {
                    try {
                        Date.valueOf(sb.getToken());
                    }
                    catch (IllegalArgumentException e) {
                        errors.rejectValue("personCriteria", "sb.person.criteria.invalid", "Invalid date format");
                    }
                }
                if(column == PersonColumns.GROUP_ID) {
                    try {
                        Integer.valueOf(sb.getToken());
                    }
                    catch (NumberFormatException e) {
                        errors.rejectValue("personCriteria", "sb.person.criteria.invalid", "Invalid group id format");
                    }
                }
            }
            catch (IllegalArgumentException e) {
                errors.rejectValue("personCriteria", "sb.person.criteria.unknown", "Unknown criteria for person");
            }
        }
        else {
            errors.rejectValue("type", "sb.type.unknown", "Unkown search type");
        }
    }

}
