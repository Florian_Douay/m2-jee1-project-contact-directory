<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>

<cus:header pageTitle="${person.firstName} ${person.lastName} contact">
    <!--Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
</cus:header>

<body>
    <main class="container-fluid">
        <nav>
            <%@ include file="/WEB-INF/fragments/menu.jsp" %>
        </nav>
        <c:choose>
            <c:when test="${sessionScope['scopedTarget.userInSession'] != null}">
                <h1 class="text-center">My profile</h1>
            </c:when>
            <c:otherwise>
                <h1 class="text-center">${person.lastName} ${person.firstName}</h1>
        	</c:otherwise>
        </c:choose>
        <div class="center-block">
            <table class="table table-bordered text-center">
                <tr>
                    <th class="text-center">Number</th><td><c:out value="${person.id}" /></td>
                </tr>
                <tr>
                    <th class="text-center">Lastname</th><td><c:out value="${person.lastName}" /></td>
                </tr>
                <tr>
                    <th class="text-center">Firstname</th><td><c:out value="${person.firstName}" /></td>
                </tr>
                <c:if test="${sessionScope['scopedTarget.userInSession'] != null}">
                    <tr>
                        <th class="text-center">E-mail</th><td><c:out value="${person.email}" /></td>
                    </tr>
                    <tr>
                        <th class="text-center">Birthdate</th><td><c:out value="${person.birthdate}" /></td>
                    </tr>
                </c:if>
                <tr>
                    <th class="text-center">Website</th><td><c:out value="${person.website}" /></td>
                </tr>
                <tr>
                    <th class="text-center">Group name</th>
                    <td><a href="${groupPage}?id=${person.groupId}"><c:out value="${group.name}" /></a></td>
                </tr>
            </table>
        </div>
        <div class="text-center">
            <c:if test="${sessionScope['scopedTarget.userInSession'].id == person.id}">
                <a class="btn btn-success" href="${editPersonPage}?id=${person.id}"><span class="glyphicon glyphicon-pencil"></span> Edit my profile</a>
            </c:if>
        </div>
    </main>
</body>
</html>
