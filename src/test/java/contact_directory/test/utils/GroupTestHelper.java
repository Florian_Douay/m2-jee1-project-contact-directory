package contact_directory.test.utils;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import contact_directory.dao.Group;

@Service("groupTestHelper")
@Scope("prototype")
public class GroupTestHelper extends Group {

    @Override
    public void setId(Long id) {
        super.setId(id);
    }
    
}
