package contact_directory.business;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import contact_directory.dao.Group;
import contact_directory.dao.PersonDao;
import contact_directory.dao.exception.DAOException;

@Service("groupManager")
public class GroupManagerWithoutRestriction implements GroupManager {

    @Autowired
    @Qualifier("personDaoJdbc")
    private PersonDao dao;
    
    @Override
    public Collection<Group> findAllGroups() throws DAOException {
        return dao.findAllGroups();
    }

    @Override
    public Collection<Group> searchForGroups(String keyword) throws DAOException {
        return dao.searchForGroups(keyword);
    }

    @Override
    public Group findGroup(long id) throws DAOException {
        return dao.findGroup(id);
    }

    @Override
    public void saveGroup(Group g) throws DAOException {
        dao.saveGroup(g);
        
    }

}
