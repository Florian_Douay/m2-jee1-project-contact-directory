package contact_directory.web.controller;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import contact_directory.business.GroupManager;
import contact_directory.business.PersonManager;
import contact_directory.dao.Group;
import contact_directory.dao.Person;
import contact_directory.dao.exception.DAOException;
import contact_directory.web.bean.PersonBean;
import contact_directory.web.bean.UserInSession;
import contact_directory.web.validators.PersonValidator;

@Controller
@RequestMapping("/persons")
public class PersonController {
    
    @Autowired
    private GroupManager groupManager;
    @Autowired
    private PersonManager personManager;
    @Autowired
    private PersonValidator validator;
    @Autowired
    private UserInSession userInSession;

    protected final Log logger = LogFactory.getLog(getClass());
    
    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public ModelAndView showGroup(@RequestParam(value = "id") Long value) throws DAOException {
        logger.info("show person");
        Person person = null;
        Group group = null;
 
        person = personManager.findPerson(value);
        group = groupManager.findGroup(person.getGroupId());

        return new ModelAndView("person").addObject("person", person).addObject("group", group);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam(value = "id") Long value) throws DAOException {
        if(userInSession == null || userInSession.getId() != value) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error");
            modelAndView.setStatus(HttpStatus.FORBIDDEN);
            return modelAndView;
        }
        //Do we need this check or do we trust session ?
        Person p = personManager.findPerson(value);
        logger.info("show edit person");
        return new ModelAndView("editPerson").addObject("person",p);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView edit(@ModelAttribute PersonBean pb, BindingResult result) throws DAOException {
        if(userInSession == null) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error");
            modelAndView.setStatus(HttpStatus.FORBIDDEN);
            return modelAndView;
        }
        
        validator.validate(pb, result);
        if (result.hasErrors()) {
            return new ModelAndView("editPerson");
        }
        
        Person p = personManager.findPerson(userInSession.getId());
        long id = p.getId();
        p.setLastName(pb.getLastName());
        p.setFirstName(pb.getFirstName());
        p.setEmail(pb.getEmail());
        p.setWebsite(pb.getWebsite());
        p.setBirthdate(Date.valueOf(pb.getBirthdate()));
        
        personManager.savePerson(p);
        
        Person updatedP = personManager.findPerson(id);
        Group group = groupManager.findGroup(updatedP.getGroupId());
        
        logger.info("Returning person view updated");
        return new ModelAndView("person").addObject("person", updatedP).addObject("group", group);
    }
    
    @ModelAttribute
    public PersonBean newPersonBean(@RequestParam(value = "lastname", required = false) String lastname,
                                    @RequestParam(value = "firstname", required = false) String firstname,
                                    @RequestParam(value = "email", required = false) String email,
                                    @RequestParam(value = "website", required = false) String website,
                                    @RequestParam(value = "birthdate", required = false) String birthdate) {
        
        PersonBean p = new PersonBean();
        p.setLastName(lastname);
        p.setFirstName(firstname);
        p.setEmail(email);
        p.setWebsite(website);
        p.setBirthdate(birthdate);
        logger.info("new person bean = " + p);
        return p;
    }

}
