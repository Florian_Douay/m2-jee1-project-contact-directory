package contact_directory.business;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import contact_directory.dao.Person;
import contact_directory.dao.PersonDao;
import contact_directory.dao.exception.DAOException;
import contact_directory.web.bean.LoginBean;

/**
 * Passwords are stored in plaintext because security is out of the scope of
 * this project. We are well aware that real passwords must be hashed and
 * salted. Hashing with a key derivation function is even better.
 */
@Service
public class LoginManagerPlainTextPassword implements LoginManager {

    @Autowired
    @Qualifier("personDaoJdbc")
    private PersonDao dao;

    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public Person login(LoginBean loginBean) {
        try {
            Person person = dao.findPersonByEmail(loginBean.getEmail());
            if(person.getPassword().equals(loginBean.getPassword())) { // We know it's also vulnerable to timing attacks!
                return person;
            } else {
                logger.info("password doesn't match for user: " + loginBean.getEmail());
                return null;
            }
        } catch (DAOException e) {
            logger.info("person with email "+ loginBean.getEmail() + " not found (or maybe another error...", e);
            return null;
        }
    }

}
