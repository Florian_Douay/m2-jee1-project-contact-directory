package contact_directory.business;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import contact_directory.dao.Person;
import contact_directory.dao.PersonColumns;
import contact_directory.dao.PersonDao;
import contact_directory.dao.exception.DAOException;

@Service("personManager")
public class PersonManagerWithoutRestriction implements PersonManager {

    @Autowired
    @Qualifier("personDaoJdbc")
    private PersonDao dao;
    
    @Override
    public Collection<Person> findAllPersons(long groupId) throws DAOException {
        return dao.findAllPersons(groupId);
    }

    @Override
    public Collection<Person> searchForPersons(String keyword, PersonColumns field) throws DAOException {
        return dao.searchForPersons(keyword, field);
    }

    @Override
    public Person findPerson(long id) throws DAOException {
        return dao.findPerson(id);
    }

    @Override
    public void savePerson(Person p) throws DAOException {
        dao.savePerson(p);
    }

}
