package contact_directory.test.utils;

import java.sql.Connection;

import org.springframework.stereotype.Repository;

import contact_directory.dao.JdbcTools;
import contact_directory.dao.exception.DAOException;

//Helper to allow testing on protected method newConnection
@Repository
public class JdbcToolsTestHelper extends JdbcTools {
  @Override
  public Connection getConnection() throws DAOException {
      return super.getConnection();
  }

  @Override
  public void close() throws DAOException {
      super.close();
  }
  
}
