package contact_directory.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import contact_directory.dao.exception.DAOException;
import contact_directory.dao.exception.EntityNotFoundException;

public abstract class JdbcTools {

    private final Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    private BasicDataSource dataSource;

    public BasicDataSource getDataSource() {
        return dataSource;
    }
    public void setDataSource(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void finalize() throws Throwable {
        logger.info("finalize");
        super.finalize();
        if(dataSource != null) {
            dataSource.close();
        }
    }
    protected void close() throws DAOException {
        logger.info("close");
        try {
            dataSource.close();
        }
        catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    protected Connection getConnection() throws DAOException {
        logger.debug("get connection");
        try {
            return dataSource.getConnection();
        }
        catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    /**
     * Do not use with an INSERT with multiple rows to insert
     * @param query
     * @param parameters
     * @return generated key in case of insert. Number of affected rows otherwise.
     * @throws DAOException
     */
    protected long executeUpdate(String query, Object... parameters) throws DAOException {
        try(Connection conn = getConnection()) {
            // prepare the instruction
            PreparedStatement st = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);

            // insert arguments in the prepared statement
            for(int i=0 ; i<parameters.length ; ++i) {
                st.setObject(i+1, parameters[i]);
            }
            // execute the instruction
            int nb = st.executeUpdate();
            
            ResultSet keys = st.getGeneratedKeys();
            if(keys.next() == true) {
                // returns the generated id in case of insert
                long generatedId = keys.getLong(1);
                st.close();
                return generatedId;
            }
            // returns the number of rows affected otherwise
            st.close();
            return nb;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(),e);
        }
    }

    protected <T> Collection<T> findBeans(String sql, ResultSetToBean<T> mapper, Object...parameters) throws DAOException {
        try (Connection conn = getConnection()) {
            PreparedStatement st = conn.prepareStatement(sql);

            for(int i=0 ; i<parameters.length ; ++i) {
                st.setObject(i+1, parameters[i]);
            }
            ResultSet rs = st.executeQuery();

            List<T> list = new ArrayList<>();
            while(rs.next()) {
                T t = mapper.toBean(rs);
                list.add(t);
            }
            st.close();
            return list;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(),e);
        }
    }

    protected <T> T findBean(String sql, ResultSetToBean<T> mapper, Object...parameters) throws DAOException {
        try (Connection conn = getConnection()) {
            PreparedStatement st = conn.prepareStatement(sql);

            for(int i=0 ; i<parameters.length ; ++i) {
                st.setObject(i+1, parameters[i]);
            }
            ResultSet rs = st.executeQuery();
            boolean resultSetIsEmpty = !rs.next();
            if(resultSetIsEmpty) {
                throw new EntityNotFoundException();
            }
            T t = mapper.toBean(rs);
            st.close();
            return t;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(),e);
        }
    }

}
