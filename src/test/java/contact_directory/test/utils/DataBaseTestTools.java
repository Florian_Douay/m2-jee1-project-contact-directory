package contact_directory.test.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import contact_directory.dao.exception.DAOException;

@Service
public class DataBaseTestTools {
    
    @Autowired
    private PersonDaoJdbcTestHelper dao;
    
    public void createTables() throws DAOException {
        String requestCreatePersons = "CREATE TABLE persons("
                            + "id INTEGER NOT NULL AUTO_INCREMENT,"
                            + "firstname VARCHAR(50) NOT NULL,"
                            + "lastname VARCHAR(50) NOT NULL,"
                            + "email VARCHAR(200) UNIQUE,"
                            + "website VARCHAR(255),"
                            + "birthdate VARCHAR(50) NOT NULL,"
                            + "password VARCHAR(80) NOT NULL,"
                            + "group_id INTEGER NOT NULL,"
                            + "PRIMARY KEY(id),"
                            + "CONSTRAINT fk_personGroup FOREIGN KEY (group_id) REFERENCES groups(id));";
        String requestCreateGroups = "CREATE TABLE groups("
                + "id INTEGER NOT NULL AUTO_INCREMENT,"
                + "name VARCHAR(50) NOT NULL,"
                + "PRIMARY KEY(id));";
        dao.executeUpdate(requestCreateGroups);
        dao.executeUpdate(requestCreatePersons);
    }

    public void dropTables() {
        String requestDropPersons = "DROP TABLE persons;";
        String requestDropGroups = "DROP TABLE groups;";
        try {dao.executeUpdate(requestDropPersons);} catch (Exception e) { System.err.println("Failed to drop table persons. Might be not existing."); }
        try {dao.executeUpdate(requestDropGroups);} catch (Exception e) { System.err.println("Failed to drop table groups. Might be not existing."); }
    }
}
