package contact_directory.business;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import contact_directory.dao.Person;
import contact_directory.test.utils.DbSetupper;
import contact_directory.test.utils.DbUnitDataSet;
import contact_directory.web.bean.LoginBean;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = "/spring-test.xml")
public class LoginManagerTest {

    @Autowired
    DbSetupper dbSetupper;

    @Autowired
    private LoginManagerPlainTextPassword loginManager;

    @Before
    public void setUp() throws Exception {
        dbSetupper.setupDbForTests(DbUnitDataSet.ONE_USER_ONE_GROUP);
    }

    @Test
    public void testLogin() {
        LoginBean loginBean = new LoginBean("email@example.org", "password");
        Person person = loginManager.login(loginBean);
        assertNotNull(person);
        assertEquals("email@example.org", person.getEmail());
    }

    @Test
    public void testLoginWrongPassword() {
        Person person = loginManager.login(new LoginBean("email@example.org", "WRONG password"));
        assertNull(person);
    }

    @Test
    public void testLoginNonExistingPerson() {
        Person person = loginManager.login(new LoginBean("NON-EXISTING@example.org", "password"));
        assertNull(person);
    }

}
