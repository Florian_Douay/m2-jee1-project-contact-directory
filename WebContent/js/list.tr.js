

function addPersonRowHandlers() {
    var table = document.getElementById("tablePersonList");
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) 
            {
                return function() { 
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
                                        window.location = pageLinkPersons + "?id=" + id;
                                 };
            };
        currentRow.onclick = createClickHandler(currentRow);
    }
}

function addGroupRowHandlers() {
    var table = document.getElementById("tableGroupList");
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) 
            {
                return function() {
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
                                        window.location = pageLinkGroups + "?id=" + id;
                                 };
            };
        currentRow.onclick = createClickHandler(currentRow);
    }
}
