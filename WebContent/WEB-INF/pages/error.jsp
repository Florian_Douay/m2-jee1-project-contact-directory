<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>

<cus:header pageTitle="Page not found">
    <!--Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
</cus:header>

<body>
    <main class="container-fluid">
        
        <section class="row margin-top-error">
        <h2 class="text-center">Oops ! We can't find the page you're looking for !</h2>
        <div class="text-center margin-top-error-button">
            <a class="btn btn-success" href="${searchPage}">Get back to Home page</a>
        </div>
        </section>
    </main>
</body>
</html>
