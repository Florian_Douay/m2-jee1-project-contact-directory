package contact_directory.dao.exception;

import java.sql.SQLException;

@SuppressWarnings("serial")
public class DAOException extends Exception {

    private String message;
    private int errorCode;

    @Override
    public String getMessage() {
        return message;
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, SQLException cause) {
        super(message, cause);
        errorCode =  cause.getErrorCode();
    }

    public DAOException() {
        super();
    }

    public int getErrorCode() {
        return errorCode;
    }

}
