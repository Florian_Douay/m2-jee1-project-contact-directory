package contact_directory.web.bean;

public class SearchBean {

    private String token;
    private String type;
    private String personCriteria;
    private String groupCriteria;
    
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getPersonCriteria() {
        return personCriteria;
    }
    public void setPersonCriteria(String personCriteria) {
        this.personCriteria = personCriteria;
    }
    public String getGroupCriteria() {
        return groupCriteria;
    }
    public void setGroupCriteria(String groupCriteria) {
        this.groupCriteria = groupCriteria;
    }
    
}
