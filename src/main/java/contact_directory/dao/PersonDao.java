package contact_directory.dao;

import java.util.Collection;

import contact_directory.dao.exception.DAOException;

public interface PersonDao {

    // retrives groups
    Collection<Group> findAllGroups() throws DAOException;

    // retrives persons
    Collection<Person> findAllPersons(long groupId) throws DAOException;

    // constructs a list of groups matching the given criteria
    Collection<Group> searchForGroups(String keyword) throws DAOException;
    
    // constructs a list of persons matching the given criteria
    Collection<Person> searchForPersons(String keyword, PersonColumns field) throws DAOException;

    // reads a person
    Person findPerson(long id) throws DAOException;

    // reads a person
    Person findPersonByEmail(String email) throws DAOException;

    // reads a person
    Group findGroup(long id) throws DAOException;

    // inserts or update a person
    void savePerson(Person p) throws DAOException;

    // inserts or updates a group
    void saveGroup(Group g) throws DAOException;


}
