package contact_directory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contact_directory.dao.Group;
import contact_directory.dao.Person;
import contact_directory.dao.PersonDaoJdbc;
import contact_directory.dao.exception.DAOException;
import contact_directory.populator.DatabasePopulator;
import contact_directory.populator.exception.DatabasePopulatorException;
import contact_directory.test.utils.DbSetupper;
import contact_directory.test.utils.DbUnitDataSet;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/spring-test.xml")
public class DatabasePopulatorTest {

    @Autowired
    @Qualifier("personDaoJdbc")
    private PersonDaoJdbc personDao;
    @Autowired
    private DatabasePopulator populator;
    
    @Autowired
    DbSetupper dbSetupper;

    @Before
    public void setUp() throws Exception {
        dbSetupper.setupDbForTests(DbUnitDataSet.EMPTY_DB);
    }

    @Test
    public void testPopulateNormalConditions1() throws DAOException, DatabasePopulatorException {
        testPopulate(1, 1, 0, 50);  //one group, one person
    }
    
    @Test
    public void testPopulateNormalConditions2() throws DAOException, DatabasePopulatorException {
        testPopulate(2,2,1,1);      //one group, two persons, one person per group
    }
    
    @Test
    public void testPopulateNormalConditions3() throws DAOException, DatabasePopulatorException {
        testPopulate(2,1,0,1);      //two group, one person, zero to one person per group
    }
    
    @Test
    public void testPopulateNormalConditions4() throws DAOException, DatabasePopulatorException {
        int nbGroups = 101;
        int nbPersons = 1025;
        int maxPersonsPerGroup = (int)Math.ceil(nbPersons/(double)nbGroups)*2;
        testPopulate(nbGroups,nbPersons,0,maxPersonsPerGroup);
    }

    //TODO test randomization quality
    
    //expected=PopulatorException.class but swallowed by this test strategy
    @Test
    public void testPopulateInvalidValues() throws DAOException {
        testPopulateFails(-1,1,1,1);   //negative groups not allowed
        testPopulateFails(0,1,1,1);    //zero groups not allowed
        testPopulateFails(1,-1,1,1);   //negative persons not allowed
        testPopulateFails(1,1,-1,1);   //negative minPersons not allowed
        testPopulateFails(1,1,1,-1);   //negative maxPersons not allowed
        testPopulateFails(1,11,12,11); //minPersonPerGroup > maxPersonPerGroup
        
        testPopulateFails(1,0,1,2);    //too can't create groups with minPersons>0 and not giving persons
        testPopulateFails(1,2,0,0);    //too can't create groups with minPersons>0 and not giving persons
        
        testPopulateFails(1,11,10,10); //too many persons for this number of groups (maxPersonPerGroup)
        testPopulateFails(5,26,1,5);   //too many persons for this number of groups (maxPersonPerGroup)
        
        testPopulateFails(1,1,2,8);    //too many groups for this number of persons, according to minPersonPerGroup
        testPopulateFails(4,7,2,8);    //too many groups for this number of persons, according to minPersonPerGroup
    }
    
    private void testPopulate(int nGroups, int nPersons, int minPersonsPerGroup, int maxPersonsPerGroup) throws DAOException, DatabasePopulatorException {
        
        populator.populate(nGroups, nPersons, minPersonsPerGroup, maxPersonsPerGroup);
        
        Collection<Group> groups = personDao.findAllGroups();
        ArrayList<Person> persons = new ArrayList<>();
        
        for(Group group : groups) {
            personDao.findAllPersons(group.getId()).forEach(person -> persons.add(person));
        }
        
        assertEquals(nGroups, groups.size());
        assertEquals(nPersons, persons.size());
        
        checkMinAndMaxPersonsPerGroup(minPersonsPerGroup, maxPersonsPerGroup, groups, persons);
    }
    
    private static void checkMinAndMaxPersonsPerGroup(int minPersonsPerGroup, int maxPersonsPerGroup, Collection<Group> groups, ArrayList<Person> persons) {
        
        //<GroupId,personsNumber>
        HashMap<Long,Integer> personsNumber = new HashMap<>();
        groups.forEach(g -> personsNumber.put(g.getId(), 0));
        
        for(Person person : persons) {
            int previousPersonsNumber = personsNumber.get(person.getGroupId());
            personsNumber.put(person.getGroupId(), previousPersonsNumber+1);
        }
        
        personsNumber.forEach( (k,v) -> assertTrue(v >= minPersonsPerGroup && v <= maxPersonsPerGroup) );
    }

    private void testPopulateFails(int nGroups, int nPersons, int minPersonsPerGroup, int maxPersonsPerGroup) throws DAOException {
        try {
            populator.populate(nGroups, nPersons, minPersonsPerGroup, maxPersonsPerGroup);
            fail("Should have thrown PopulatorException");
        }
        catch (DatabasePopulatorException e) {}
        catch (DAOException e) { throw e; }
    }
    
}
