<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<cus:header pageTitle="Search">
    <!--Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
    <script type="text/javascript">var pageLinkGroups = "${groupPage}";</script>
    <script type="text/javascript">var pageLinkPersons = "${personPage}";</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/list.tr.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/search.fields.js"></script>
</cus:header>

<body>
    <main class="container-fluid">
    
        <nav>
            <%@ include file="/WEB-INF/fragments/menu.jsp"%>
        </nav>
    
        <section>
            <h2 class="text-center">Search</h2>
            <form:form method="POST" commandName="searchBean">
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="token" class="col-form-label">Token</label>
                        <form:input path="token" class="form-control"/>
                        <span class="help-block error"><form:errors path="token"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="type" class="col-form-label">Kind</label>
                        <form:select path="type" multiple="false" onchange="updateType()" class="form-control">
                            <form:option value="" label="--- Select ---" />
                            <form:options items="${searchType}" />
                        </form:select>
                        <span class="help-block error"><form:errors path="type"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                            <label for="groupCriteria" id="groupCriteriaLabel" class="col-form-label">Criteria</label>
                            <label for="personCriteria" id="personCriteriaLabel" class="col-form-label">Criteria</label>
                            <div id="groupCriteriaContainer">
                                <form:select path="groupCriteria" multiple="false" class="form-control">
                                    <form:options items="${searchGroupCriterias}" />
                                </form:select>
                                <span class="help-block error"><form:errors path="groupCriteria"/></span>
                            </div>
                            <div id="personCriteriaContainer">
                                <form:select path="personCriteria" multiple="false" class="form-control">
                                    <form:option value="" label="--- Select ---" />
                                    <form:options items="${searchPersonCriterias}" />
                                </form:select>
                                <span class="help-block error"><form:errors path="personCriteria"/></span>
                            </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <input type="submit" class="btn btn-default" />
                    </div>
                </div>
                </form:form>
        </section>
    
        <c:if test="${emptyResults == true}">
            <section>
                <h2 class="text-center">Results</h2>
                <div class="row center-block">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <p class="no-result-block">
                           <c:out value="No result found" />
                        </p>
                    </div>
                </div>
            </section>
        </c:if>
    
        <c:if test="${groups.isEmpty() == false}">
            <section>
                <h2 class="text-center">Number of results</h2>
                <div class="row center-block">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <p class="result-block">
                           <c:out value="${groups.size()}" />
                           <c:out value=" groups found" />
                        </p>
                    </div>
                </div>
            </section>
            <section class="row">
                <h2 class="text-center">Results list</h2>
                <div class="search-result center-block">
                    <table id="tableGroupList" class="table table-bordered text-center">
                        <tr>
                            <th class="text-center">Numero</th>
                            <th class="text-center">Name</th>
                        </tr>
                        <c:forEach items="${groups}" var="gr">
                            <tr class="clickable-row">
                                <td><c:out value="${gr.id}" /></td>
                                <td><c:out value="${gr.name}" /></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </section>
        </c:if>
        <c:if test="${persons.isEmpty() == false}">
            <section>
                <h2 class="text-center">Number of results</h2>
                <div class="row center-block">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <p class="result-block">
                           <c:out value="${persons.size()}" />
                           <c:out value=" persons found" />
                        </p>
                    </div>
                </div>
            </section>
            <section class="row">
                <h2 class="text-center">Results list</h2>
                <div class="search-result center-block">
                    <table id="tablePersonList" class="table table-bordered text-center">
                        <tr>
                            <th class="text-center">Numero</th>
                            <th class="text-center">Lastname</th>
                            <th class="text-center">Firstname</th>
                            <c:if test="${sessionScope['scopedTarget.userInSession'] != null}">
                                <th class="text-center">Email</th>
                                <th class="text-center">Birthday</th>
                            </c:if>
                            <th class="text-center">Website</th>
                        </tr>
                        <c:forEach items="${persons}" var="per">
                            <tr class="clickable-row">
                                <td><c:out value="${per.id}" /></td>
                                <td><c:out value="${per.lastName}" /></td>
                                <td><c:out value="${per.firstName}" /></td>
                                <c:if test="${sessionScope['scopedTarget.userInSession'] != null}">
                                    <td><c:out value="${per.email}" /></td>
                                    <td><c:out value="${per.birthdate}" /></td>
                                </c:if>
                                <td><c:out value="${per.website}" /></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </section>
        </c:if> 
    </main>
</body>
</html>