package contact_directory.test.utils;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import contact_directory.dao.PersonDaoJdbc;
import contact_directory.dao.exception.DAOException;

//Helper to allow testing using protected method executeUpdate
@Repository
@Qualifier("personDaoJdbcTestHelper")
@Scope("prototype")
public class PersonDaoJdbcTestHelper extends PersonDaoJdbc {

  @Override
  public long executeUpdate(String query, Object... parameters) throws DAOException {
      return super.executeUpdate(query, parameters);
  }

}
