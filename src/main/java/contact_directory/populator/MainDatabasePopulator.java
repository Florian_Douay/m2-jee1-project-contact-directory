package contact_directory.populator;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import contact_directory.dao.exception.DAOException;
import contact_directory.populator.exception.DatabasePopulatorException;

public class MainDatabasePopulator {

    public static void main(String[] args) throws DAOException, DatabasePopulatorException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        DatabasePopulator populator = context.getBean(DatabasePopulator.class);
        int nGroups = 100;
        int nPersons = 1000;
        int minPersonsPerGroup = 0;
        int maxPersonsPerGroup = 20;
        populator.populate(nGroups, nPersons, minPersonsPerGroup, maxPersonsPerGroup);
        context.close();
    }

}
