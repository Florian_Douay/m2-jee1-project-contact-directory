# Dependencies
- MariaDB or MySQL
- Maven
- Java Servlet Container

# Setup dev environment
## Create DB
CREATE DATABASE mydbname;

## Create User
GRANT ALL PRIVILEGES on mydbname.* to 'myuser'@'localhost' IDENTIFIED BY 'mypass';

## Create the database properties file
Copy the properties.sample and edit the fields according to your database

`cp src/main/resources/jdbc.properties.sample src/main/resources/jdbc.properties`

Fill the config file with the values according to your DB

# Usage
## Web app
Run the app with a Java EE Servlet Container. We used Tomcat 8.0x. Olders servers might work.

## Populator : filling the DB with sample data
Run MainDatabasePopulator from your IDE or from the Maven command line :

`mvn exec:java -Dexec.mainClass="contact_directory.populator.MainDatabasePopulator"`

## Run the tests
`mvn test`
