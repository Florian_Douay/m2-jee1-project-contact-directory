<%@ include file="/WEB-INF/fragments/include.jsp" %>

<!-- the use of sessionScope is a violation of the separation of concerns,
ideally, the view shouldn't be aware of the session. The controller should
set a boolean in this case to indicate if the person is logged in.
But, no simple way to add a ModelAttribute to *all* controllers was found -->

<div class="btn-toolbar row">
    <div class="col-md-4">
      <div class="btn-group btn-group-justified">
      <a class="btn btn-success" href="${searchPage}"><span class="glyphicon glyphicon-search"></span><br>Search</a>
      <a class="btn btn-warning" href="${groupsListPage}"><span class="glyphicon glyphicon-book"></span><br>Groups</a>
    </div>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <div class="btn-group btn-group-justified">
            <c:choose>
                <c:when test="${sessionScope['scopedTarget.userInSession'] != null}">
                        <a class="btn btn-info" href="${groupPage}?id=${sessionScope['scopedTarget.userInSession'].groupId}"><span class="glyphicon glyphicon-th-large"></span><br>My group</a>
                        <a class="btn btn-primary" href="${personPage}?id=${sessionScope['scopedTarget.userInSession'].id}"><span class="glyphicon glyphicon-user"></span><br>Profile</a>
                        <!-- Log out must not be a GET, see the semantic of GET to understand why.
                             See also browser prefetch -->
                        <form id="submit" class="btn btn-danger" method="post" action="${logoutPage}" onclick="submitForm();">
                            <span class="glyphicon glyphicon-lock"></span>
                            <br>Log out
                        </form>
                </c:when>
                <c:otherwise>
                    <a class="btn btn-danger" href="${loginPage}"><span class="glyphicon glyphicon-lock"></span><br>Log in</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
