<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>

<cus:header pageTitle="Group">
    <!--Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
    <script type="text/javascript">var pageLinkPersons = "${personPage}";</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/list.tr.js"></script>
    <script type="text/javascript">window.onload = addPersonRowHandlers;</script>
</cus:header>

<body>
    <main class="container-fluid">
        <nav>
            <%@ include file="/WEB-INF/fragments/menu.jsp" %>
        </nav>
        
        <section class="row">
        <h2 class="text-center">${group.name}'s members</h2>
        <div class="person-list center-block">
            <table id="tablePersonList" class="table table-bordered text-center">
                <tr>
                    <th class="text-center">Number</th>
                    <th class="text-center">Lastname</th>
                    <th class="text-center">Firstname</th>
                    <c:if test="${sessionScope['scopedTarget.userInSession'] != null}">
                        <th class="text-center">Email</th>
                        <th class="text-center">Birthday</th>
                    </c:if>
                    <th class="text-center">Website</th>
                </tr>
                <c:forEach items="${persons}" var="per">
                    <tr class="clickable-row">
                        <td><c:out value="${per.id}" /></td>
                        <td><c:out value="${per.lastName}" /></td>
                        <td><c:out value="${per.firstName}" /></td>
                        <c:if test="${sessionScope['scopedTarget.userInSession'] != null}">
                            <td><c:out value="${per.email}" /></td>
                            <td><c:out value="${per.birthdate}" /></td>
                        </c:if>
                        <td><c:out value="${per.website}" /></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        </section>
    </main>
</body>
</html>
