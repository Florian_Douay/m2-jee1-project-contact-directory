package contact_directory.dao;

public enum PersonColumns {

    ID {
        @Override
        public String toString() {
            return "id";
        }
    },
    LAST_NAME {
        @Override
        public String toString() {
            return "lastname";
        }
    },
    FIRST_NAME {
        @Override
        public String toString() {
            return "firstname";
        }
    },
    EMAIL {
        @Override
        public String toString() {
            return "email";
        }
    },
    WEBSITE {
        @Override
        public String toString() {
            return "website";
        }
    },
    BIRTH_DATE {
        @Override
        public String toString() {
            return "birthdate";
        }
    },
    PASSWORD {
        @Override
        public String toString() {
            return "password";
        }
    },
    GROUP_ID {
        @Override
        public String toString() {
            return "group_id";
        }
    };

}
