package contact_directory.web.validators;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import contact_directory.web.bean.PersonBean;

@Service
public class PersonValidator implements Validator {

    protected final Log logger = LogFactory.getLog(getClass());
    
    @Override
    public boolean supports(Class<?> clazz) {
        return PersonBean.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        
        PersonBean p = (PersonBean) target;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "p.lastname", "Lastname is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "p.firstname", "Firstname is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "p.email", "Email is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "website", "p.website", "Website is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthdate", "p.birthdate", "Birthdate is required.");
        
        try {
            Date.valueOf(p.getBirthdate());
        }
        catch (IllegalArgumentException e) {
            errors.rejectValue("birthdate", "p.birthdate.invalid", "Invalid date format");
        }
    }
    
}
