package contact_directory.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetToGroup implements ResultSetToBean<Group> {

    //constructs a Group from a ResultSet
    @Override
    public Group toBean(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        String name = rs.getString("name");

        Group g = new Group();
        g.setId(id);
        g.setName(name);
        return g;
    }

}
