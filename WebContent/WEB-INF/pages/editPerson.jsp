<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<cus:header pageTitle="Edit profile">
    <!--Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
</cus:header>

<body>
    <main class="container-fluid">
    
        <nav>
            <%@ include file="/WEB-INF/fragments/menu.jsp"%>
        </nav>
    
        <section>
            <h2 class="text-center">Edit profile</h2>
            <form:form method="POST" commandName="personBean">
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="lastName" class="col-form-label">Lastname</label>
                        <form:input path="lastName" class="form-control" value="${person.lastName}"/>
                        <span class="help-block error"><form:errors path="lastName"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="firstName" class="col-form-label">Firstname</label>
                        <form:input path="firstName" class="form-control" value="${person.firstName}"/>
                        <span class="help-block error"><form:errors path="firstName"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="email" class="col-form-label">Email</label>
                        <form:input path="email" class="form-control" value="${person.email}"/>
                        <span class="help-block error"><form:errors path="email"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="website" class="col-form-label">Website</label>
                        <form:input path="website" class="form-control" value="${person.website}"/>
                        <span class="help-block error"><form:errors path="website"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3">
                        <label for="birthdate" class="col-form-label">Birthdate</label>
                        <form:input path="birthdate" class="form-control" value="${person.birthdate}"/>
                        <span class="help-block error"><form:errors path="birthdate"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <input type="submit" class="btn btn-default" />
                    </div>
                </div>
                </form:form>
        </section>
    </main>

</body>
</html>