package contact_directory.dao;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import contact_directory.dao.exception.DAOException;

@Repository
@Qualifier("personDaoJdbc")
public class PersonDaoJdbc extends JdbcTools implements PersonDao {
    
    private final Log logger = LogFactory.getLog(getClass());
    
    @Override
    public Collection<Group> findAllGroups() throws DAOException {
        String request = "SELECT * FROM groups;";
        ResultSetToGroup rsToGroup = new ResultSetToGroup();
        logger.info("find all groups");
        return findBeans(request,rsToGroup);
    }

    @Override
    public Collection<Person> findAllPersons(long groupId) throws DAOException {
        //checks if the group is existing, throws exception otherwise
        findGroup(groupId);

        String request = "SELECT * FROM persons WHERE " + PersonColumns.GROUP_ID + "=?;";
        ResultSetToPerson rsToPerson = new ResultSetToPerson();
        logger.info("find all persons");
        return findBeans(request,rsToPerson,groupId);
    }

    @Override
    public Collection<Person> searchForPersons(String keyword, PersonColumns field) throws DAOException {
        String request = "SELECT * FROM persons WHERE " + field.toString() + " LIKE ?;";
        ResultSetToPerson rsToPerson = new ResultSetToPerson();
        logger.info("search for persons");
        return findBeans(request,rsToPerson,"%" + keyword + "%");
    }

    @Override
    public Collection<Group> searchForGroups(String keyword) throws DAOException {
        String request = "SELECT * FROM groups WHERE name LIKE ?;";
        ResultSetToGroup rsToGroup = new ResultSetToGroup();
        logger.info("search for groups");
        return findBeans(request,rsToGroup,"%" + keyword + "%");
    }

    @Override
    public Person findPerson(long id) throws DAOException {
        String request = "SELECT * FROM persons WHERE " + PersonColumns.ID + "=?;";
        ResultSetToPerson rsToPerson = new ResultSetToPerson();
        logger.info("find person");
        return findBean(request,rsToPerson,id);
    }

    @Override
    public Person findPersonByEmail(String email) throws DAOException {
        String request = "SELECT * FROM persons WHERE " + PersonColumns.EMAIL + "=?;";
        ResultSetToPerson rsToPerson = new ResultSetToPerson();
        logger.info("find by email");
        return findBean(request,rsToPerson,email);
    }

    @Override
    public Group findGroup(long id) throws DAOException {
        String request = "SELECT * FROM groups WHERE id=?;";
        ResultSetToGroup rsToGroup = new ResultSetToGroup();
        logger.info("find group");
        return findBean(request,rsToGroup,id);
    }

    @Override
    public void savePerson(Person p) throws DAOException {
        String request;
        if(p.getId() == null) {
            request = "INSERT INTO persons(" + PersonColumns.FIRST_NAME + ","
                                             + PersonColumns.LAST_NAME + ","
                                             + PersonColumns.EMAIL + ","
                                             + PersonColumns.WEBSITE + ","
                                             + PersonColumns.BIRTH_DATE + ","
                                             + PersonColumns.PASSWORD + ","
                                             + PersonColumns.GROUP_ID
                                             + ") VALUES(?,?,?,?,?,?,?);";
            logger.trace("save person (add)");
            long generatedId = executeUpdate(request,
                                             p.getFirstName(),
                                             p.getLastName(),
                                             p.getEmail(),
                                             p.getWebsite(),
                                             p.getBirthdate(),
                                             p.getPassword(),
                                             p.getGroupId());
            p.setId(generatedId);
        }
        else {
            request = "UPDATE persons SET " + PersonColumns.FIRST_NAME + "=?,"
                                            + PersonColumns.LAST_NAME + "=?,"
                                            + PersonColumns.EMAIL + "=?,"
                                            + PersonColumns.WEBSITE + "=?,"
                                            + PersonColumns.BIRTH_DATE + "=?,"
                                            + PersonColumns.PASSWORD + "=?,"
                                            + PersonColumns.GROUP_ID + "=?"
                                            + " WHERE " + PersonColumns.ID + "=?;";
            logger.info("save person (update)");
            long nbAffectedRows = executeUpdate(request,
                                                p.getFirstName(),
                                                p.getLastName(),
                                                p.getEmail(),
                                                p.getWebsite(),
                                                p.getBirthdate(),
                                                p.getPassword(),
                                                p.getGroupId(),
                                                p.getId());
            if(nbAffectedRows != 1) {
                throw new DAOException("Error when updating person (id:" + p.getId() +") in database. " + nbAffectedRows + " rows affected.");
            }
        }
    }

    @Override
    public void saveGroup(Group g) throws DAOException {
        String request;
        if(g.getId() == null) {
            request = "INSERT INTO groups(name) VALUES(?);";
            logger.trace("save group (add)");
            long generatedId = executeUpdate(request, g.getName());
            g.setId(generatedId);
        }
        else {
            request = "UPDATE groups SET name=? WHERE id=?;";
            logger.info("save group (update)");
            long nbAffectedRows = executeUpdate(request, g.getName(), g.getId());
            if(nbAffectedRows != 1) {
                throw new DAOException("Error when updating group (id:" + g.getId() +") in database. " + nbAffectedRows + " rows affected.");
            }
        }
    }

}
