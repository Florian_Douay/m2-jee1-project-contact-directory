<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>

<cus:header pageTitle="Groups">
        <!--Bootstrap -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
        <script type="text/javascript">var pageLinkGroups = "${groupPage}";</script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/list.tr.js"></script>
        <script type="text/javascript">window.onload = addGroupRowHandlers;</script>
</cus:header>

    <body>
        <main class="container-fluid">
        
            <nav>
                <%@ include file="/WEB-INF/fragments/menu.jsp" %>
            </nav>
        
            <section class="row">
                <h2 class="text-center">Groups</h2>
                <div class="group-list center-block">
                    <table id="tableGroupList" class="table table-bordered text-center">
                        <tr>
                            <th class="text-center">Number</th>
                            <th class="text-center">Name</th>
                        </tr>
                        <c:forEach items="${groups}" var="gr">
                            <tr class=clickable-row>
                                <td><c:out value="${gr.id}" /></td>
                                <td><c:out value="${gr.name}" /></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </section>
        </main>
    </body>
</html>
