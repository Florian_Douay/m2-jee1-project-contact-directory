package contact_directory.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetToPerson implements ResultSetToBean<Person> {

    //constructs a Person from a ResultSet
    @Override
    public Person toBean(ResultSet rs) throws SQLException {

        long id = rs.getLong(PersonColumns.ID.toString());
        String firstName = rs.getString(PersonColumns.FIRST_NAME.toString());
        String lastName = rs.getString(PersonColumns.LAST_NAME.toString());
        String email = rs.getString(PersonColumns.EMAIL.toString());
        String website = rs.getString(PersonColumns.WEBSITE.toString());
        Date birthdate = rs.getDate(PersonColumns.BIRTH_DATE.toString());
        String password = rs.getString(PersonColumns.PASSWORD.toString());
        long idGroup = rs.getLong(PersonColumns.GROUP_ID.toString());

        Person p = new Person();

        p.setId(id);
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setEmail(email);
        p.setWebsite(website);
        p.setBirthdate(birthdate);
        p.setPassword(password);
        p.setGroupId(idGroup);

        return p;
    }

}
