package contact_directory.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetToBean<T> {
    //constructs a bean of type T from a ResultSet
    T toBean(ResultSet rs) throws SQLException;
}
