<%@ attribute name="pageTitle" required="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>${pageTitle}</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/menu.logout.submit.js"></script>
    <jsp:doBody />
</head>