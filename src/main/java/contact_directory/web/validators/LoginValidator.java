package contact_directory.web.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import contact_directory.web.bean.LoginBean;

@Service
public class LoginValidator implements Validator {

    protected final Log logger = LogFactory.getLog(getClass());
    
    @Override
    public boolean supports(Class<?> clazz) {
        return LoginBean.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LoginBean l = (LoginBean) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "l.email", "Email is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "l.password", "Password is required.");
    }
    
}
