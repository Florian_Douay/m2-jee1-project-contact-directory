package contact_directory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Date;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import contact_directory.dao.Group;
import contact_directory.dao.Person;
import contact_directory.dao.PersonColumns;
import contact_directory.dao.exception.DAOException;
import contact_directory.dao.exception.EntityNotFoundException;
import contact_directory.test.utils.DbSetupper;
import contact_directory.test.utils.DbUnitDataSet;
import contact_directory.test.utils.GroupTestHelper;
import contact_directory.test.utils.PersonDaoJdbcTestHelper;
import contact_directory.test.utils.PersonTestHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/spring-test.xml")
public class PersonDaoJDBCTest {

    @Autowired
    private PersonDaoJdbcTestHelper dao;
    @Autowired
    private PersonTestHelper personTestHelper1;
    @Autowired
    private PersonTestHelper personTestHelper2;
    @Autowired
    private PersonTestHelper personTestHelper3;
    @Autowired
    private PersonTestHelper personTestHelper4;
    @Autowired
    private GroupTestHelper groupTestHelper;
    
    @Autowired
    DbSetupper dbSetupper;

    @Before
    public void setUp() throws Exception {
        dbSetupper.setupDbForTests(DbUnitDataSet.EMPTY_DB);
    }

    @Test
    public void testSearchForPersonsByFirstname() throws DAOException {
        Group g1 = new Group("les wistiti");
        Group g2 = new Group("les chamalow");
        dao.saveGroup(g1);
        dao.saveGroup(g2);

        //alex and alexandre have the same prefix
        //ornor and eleonor have the same sufix
        //bel, anabella and abella have the same center
        //sebastien and fernand are not supposed to appear in results
        //we splited persons in two differents groups, to check group don't influence results
        Person p1g1 = new Person("iglesias", "alex", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        Person p2g1 = new Person("dupont", "onor", "email2@provider2.com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());
        Person p3g1 = new Person("boubi", "anabella", "email3@provider3.com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());
        Person p4g1 = new Person("chabal", "sebastien", "email4@provider4.com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());

        Person p1g2 = new Person("de magellan", "fernand", "email5@provider5.com", "http://www.doesntexist.pt", Date.valueOf("1521-04-24"),"spinmyhead",g2.getId());
        Person p2g2 = new Person("de vinci", "alexandre", "email6@provider6.com", "http://www.wowart.it", Date.valueOf("1324-01-08"),"davincicode",g2.getId());
        Person p3g2 = new Person("bouteflica", "eleonor", "email7@provider7.com", "http://www.wowart.it", Date.valueOf("1324-01-08"),"davincicode",g2.getId());
        Person p4g2 = new Person("ricci", "abella", "email8@provider8.com", "http://www.doesntexist.pt", Date.valueOf("1521-04-24"),"spinmyhead",g2.getId());
        Person p5g2 = new Person("ricci", "bel", "email9@provider9.com", "http://www.doesntexist.pt", Date.valueOf("1521-04-24"),"spinmyhead",g2.getId());

        dao.savePerson(p1g1);
        dao.savePerson(p2g1);
        dao.savePerson(p3g1);
        dao.savePerson(p4g1);

        dao.savePerson(p1g2);
        dao.savePerson(p2g2);
        dao.savePerson(p3g2);
        dao.savePerson(p4g2);
        dao.savePerson(p5g2);

        Collection<Person> coll = dao.searchForPersons("alex",PersonColumns.FIRST_NAME);
        assertEquals(2,coll.size());
        assertTrue(coll.contains(p2g2));
        assertTrue(coll.contains(p1g1));

        coll = dao.searchForPersons("alexandre",PersonColumns.FIRST_NAME);
        assertEquals(1,coll.size());
        assertTrue(coll.contains(p2g2));

        coll = dao.searchForPersons("onor",PersonColumns.FIRST_NAME);
        assertEquals(2,coll.size());
        assertTrue(coll.contains(p2g1));
        assertTrue(coll.contains(p3g2));

        coll = dao.searchForPersons("eleonor",PersonColumns.FIRST_NAME);
        assertEquals(1,coll.size());
        assertTrue(coll.contains(p3g2));

        coll = dao.searchForPersons("bel",PersonColumns.FIRST_NAME);
        assertEquals(3,coll.size());
        assertTrue(coll.contains(p3g1));
        assertTrue(coll.contains(p4g2));
        assertTrue(coll.contains(p5g2));
    }
    
    @Test
    public void testSearchForPersonsByBirthdate() throws DAOException {

        Group g1 = new Group("les wistiti");
        Group g2 = new Group("les chamalow");
        dao.saveGroup(g1);
        dao.saveGroup(g2);

        //onor and alexandre have the same birthdate
        Person p1g1 = new Person("iglesias", "alex", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        Person p2g1 = new Person("dupont", "onor", "email2@provider2com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());
        Person p3g1 = new Person("boubi", "anabella", "email3@provider3.com", "http://www.whynot.es", Date.valueOf("2028-05-04"),"123456",g1.getId());
        Person p4g1 = new Person("chabal", "sebastien", "email4@provider4.com", "http://www.whynot.es", Date.valueOf("2030-05-03"),"123456",g1.getId());

        Person p1g2 = new Person("de magellan", "fernand", "email5@provider5com", "http://www.doesntexist.pt", Date.valueOf("1520-04-24"),"spinmyhead",g2.getId());
        Person p2g2 = new Person("de vinci", "alexandre", "email6@provider6.com", "http://www.wowart.it", Date.valueOf("2034-05-02"),"davincicode",g2.getId());
        Person p3g2 = new Person("bouteflica", "eleonor", "email7@provider1.com", "http://www.wowart.it", Date.valueOf("1324-01-08"),"davincicode",g2.getId());
        Person p4g2 = new Person("ricci", "abella", "email8@provider8.com", "http://www.doesntexist.pt", Date.valueOf("1525-11-05"),"spinmyhead",g2.getId());
        Person p5g2 = new Person("ricci", "bel", "email9@provider9.com", "http://www.doesntexist.pt", Date.valueOf("1521-04-25"),"spinmyhead",g2.getId());

        dao.savePerson(p1g1);
        dao.savePerson(p2g1);
        dao.savePerson(p3g1);
        dao.savePerson(p4g1);

        dao.savePerson(p1g2);
        dao.savePerson(p2g2);
        dao.savePerson(p3g2);
        dao.savePerson(p4g2);
        dao.savePerson(p5g2);

        Collection<Person> coll = dao.searchForPersons("2030-05-03",PersonColumns.BIRTH_DATE);
        assertEquals(1,coll.size());
        assertTrue(coll.contains(p4g1));
        
        coll = dao.searchForPersons("2034-05-02",PersonColumns.BIRTH_DATE);
        assertEquals(2,coll.size());
        assertTrue(coll.contains(p2g1));
        assertTrue(coll.contains(p2g2));
        
        coll = dao.searchForPersons(p1g2.getBirthdate().toString(),PersonColumns.BIRTH_DATE);
        assertEquals(1,coll.size());
        assertTrue(coll.contains(p1g2));
    }

    @Test
    public void testSearchForPersonsWithNoResult() throws DAOException {
        for(PersonColumns column : PersonColumns.values()) {
            Collection<Person> coll = dao.searchForPersons("random",column);
            assertEquals(0,coll.size());
        }
    }

    @Test
    public void testSearchForGroups() throws Exception {
        Group g1 = new Group("foobar");
        Group g2 = new Group("foo");
        Group g3 = new Group("barfoo");
        Group g4 = new Group("noise foo bar noise");
        Group g5 = new Group("noise");
        dao.saveGroup(g1);
        dao.saveGroup(g2);
        dao.saveGroup(g3);
        dao.saveGroup(g4);
        dao.saveGroup(g5);

        Collection<Group> resultsFoo = dao.searchForGroups("foo");
        assertEquals(4, resultsFoo.size());
        assertTrue(resultsFoo.contains(g1));
        assertTrue(resultsFoo.contains(g2));
        assertTrue(resultsFoo.contains(g3));
        assertTrue(resultsFoo.contains(g4));

        Collection<Group> resultsNotExistingGroup = dao.searchForGroups("not exisiting group");
        assertEquals(0, resultsNotExistingGroup.size());

        Collection<Group> resultsFooBar = dao.searchForGroups("foo bar");
        assertEquals(1, resultsFooBar.size());
        assertTrue(resultsFooBar.contains(g4));
    }

    @Test
    public void testFindAllPersons() throws DAOException {
        Group g1 = new Group("les wistiti");
        Group g2 = new Group("les chamalow");
        dao.saveGroup(g1);
        dao.saveGroup(g2);

        Person p1 = new Person("iglesias", "enrique", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        Person p2 = new Person("polo", "marco", "email2@provider2com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());
        Person p3 = new Person("de magellan", "fernand", "email3@provider3com", "http://www.doesntexist.pt", Date.valueOf("1521-04-24"),"spinmyhead",g1.getId());
        Person p4 = new Person("de vinci", "leonard", "email4@provider4.com", "http://www.wowart.it", Date.valueOf("1324-01-08"),"davincicode",g2.getId());
        dao.savePerson(p1);
        dao.savePerson(p2);
        dao.savePerson(p3);
        dao.savePerson(p4);

        Collection<Person> coll1 = dao.findAllPersons(g1.getId());
        Collection<Person> coll2 = dao.findAllPersons(g2.getId());
        assertEquals(3,coll1.size());
        assertEquals(1,coll2.size());

        assertTrue(coll1.contains(p1));
        assertTrue(coll1.contains(p2));
        assertTrue(coll1.contains(p3));
        assertTrue(coll2.contains(p4));
    }

    @Test
    public void testFindPerson() throws DAOException {
        Group g1 = new Group("les wistiti");
        Group g2 = new Group("les wistiti");
        dao.saveGroup(g1);
        dao.saveGroup(g2);

        Person p1 = new Person("iglesias", "enrique", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        Person p2 = new Person("polo", "marco", "email2@provider2com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());
        Person p3 = new Person("de magellan", "fernand", "email3@provider3com", "http://www.doesntexist.pt", Date.valueOf("1521-04-24"),"spinmyhead",g1.getId());
        Person p4 = new Person("de vinci", "leonard", "email4@provider4.com", "http://www.wowart.it", Date.valueOf("1324-01-08"),"davincicode",g2.getId());
        dao.savePerson(p1);
        dao.savePerson(p2);
        dao.savePerson(p3);
        dao.savePerson(p4);

        assertTrue(dao.findPerson(p1.getId()).equals(p1));
        assertTrue(dao.findPerson(p3.getId()).equals(p3));
        assertTrue(dao.findPerson(p2.getId()).equals(p2));
        assertTrue(dao.findPerson(p4.getId()).equals(p4));
    }

    @Test
    public void testFindPersonByEmail() throws DAOException {
        Group g1 = new Group("les wistiti");
        dao.saveGroup(g1);
        Person p1 = new Person("iglesias", "enrique", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        dao.savePerson(p1);
        assertTrue(dao.findPersonByEmail(p1.getEmail()).equals(p1));
    }

    @Test(expected=DAOException.class)
    public void testFindPersonByEmailNonExisting() throws DAOException {
        dao.findPersonByEmail("nonExistingEmail@example.org");
    }

    @Test(expected=EntityNotFoundException.class)
    public void testFindPersonWithNonExistingPerson() throws DAOException {
        dao.findPerson(2);
    }

    @Test
    public void testFindGroup() throws DAOException {
        Group g  = new Group("les verts");
        dao.saveGroup(g);

        Group groupFromDb = dao.findGroup(g.getId());
        assertTrue(g.equals(groupFromDb));
    }

    @Test(expected=DAOException.class)
    public void testFindNonExistingGroup() throws DAOException {
        dao.findGroup(42);
    }

    @Test
    public void testFindAllPersonsWithEmptyGroup () throws DAOException {
        Group g = new Group("les wistiti");
        dao.saveGroup(g);

        Collection<Person> coll1 = dao.findAllPersons(g.getId());
        assertEquals(0,coll1.size());
    }

    @Test(expected=DAOException.class)
    public void testFindAllPersonsWithNonExistingGroup () throws DAOException {
        dao.findAllPersons(1);
    }

    @Test
    public void testFindAllGroups() throws DAOException {
        Group g1  = new Group("les verts");
        Group g2  = new Group("les bleus");
        Group g3  = new Group("les rouges");
        Group g4  = new Group("les jaunes");

        dao.saveGroup(g1);
        dao.saveGroup(g2);
        dao.saveGroup(g3);
        dao.saveGroup(g4);

        Collection<Group> coll = dao.findAllGroups();
        assertEquals(4,coll.size());

        assertTrue(coll.contains(g1));
        assertTrue(coll.contains(g2));
        assertTrue(coll.contains(g3));
        assertTrue(coll.contains(g4));
    }

    @Test
    public void testSaveGroupExisting() throws DAOException {
        Group g1  = new Group("les verts");
        Group g2  = new Group("les bleus");
        Group g3  = new Group("les rouges");
        Group g4  = new Group("les jaunes");

        dao.saveGroup(g1);    //insert
        dao.saveGroup(g2);    //insert
        dao.saveGroup(g3);    //insert
        dao.saveGroup(g4);    //insert

        Collection<Group> coll = dao.findAllGroups();
        assertEquals(4,coll.size());

        assertTrue(coll.contains(g1));
        assertTrue(coll.contains(g2));
        assertTrue(coll.contains(g3));
        assertTrue(coll.contains(g4));

        g1.setName("les yankees");
        g2.setName("les bulls");
        g3.setName("les red bulls");
        g4.setName("les yellow bulls");

        dao.saveGroup(g1);  //update
        dao.saveGroup(g2);  //update
        dao.saveGroup(g3);  //update
        dao.saveGroup(g4);  //update

        coll = dao.findAllGroups();

        assertEquals(4,coll.size());
        assertTrue(coll.contains(g1));
        assertTrue(coll.contains(g2));
        assertTrue(coll.contains(g3));
        assertTrue(coll.contains(g4));
    }

    @Test(expected=DAOException.class)
    public void testSaveGroupNotExistingWithPositiveId() throws DAOException {
        groupTestHelper.setName("les verts");
        groupTestHelper.setId(502L);

      //Try to save with a positive id but nothing in database
        dao.saveGroup(groupTestHelper);
    }

    @Test
    public void testSavePersonNonExisting() throws DAOException {
        //prepare groups --------------------------------------------
        Group g1  = new Group("les verts");
        Group g2  = new Group("les bleus");

        dao.saveGroup(g1);
        dao.saveGroup(g2);

        Collection<Group> collGroups = dao.findAllGroups();
        assertEquals(2,collGroups.size());

        assertTrue(collGroups.contains(g1));
        assertTrue(collGroups.contains(g2));

        //save persons --------------------------------------------
        Person p1 = new Person("iglesias", "enrique", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        Person p2 = new Person("polo", "marco", "email2@provider2com", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g1.getId());
        Person p3 = new Person("de magellan", "fernand", "email3@provider3com", "http://www.doesntexist.pt", Date.valueOf("1521-04-24"),"spinmyhead",g1.getId());
        Person p4 = new Person("de vinci", "leonard", "email4@provider4.com", "http://www.wowart.it", Date.valueOf("1324-01-08"),"davincicode",g2.getId());

        dao.savePerson(p1);
        dao.savePerson(p2);
        dao.savePerson(p3);
        dao.savePerson(p4);

        //check success --------------------------------------------
        Collection<Person> collPersonGr1 = dao.findAllPersons(g1.getId());
        assertEquals(3,collPersonGr1.size());
        Collection<Person> collPersonGr2 = dao.findAllPersons(g2.getId());
        assertEquals(1,collPersonGr2.size());

        assertTrue(collPersonGr1.contains(p1));
        assertTrue(collPersonGr1.contains(p2));
        assertTrue(collPersonGr1.contains(p3));
        assertTrue(collPersonGr2.contains(p4));
    }

    @Test
    public void testSavePersonExisting() throws DAOException {
        //prepare groups --------------------------------------------
        Group g1  = new Group("les verts");
        Group g2  = new Group("les bleus");

        dao.saveGroup(g1);
        dao.saveGroup(g2);

        Collection<Group> collGroups = dao.findAllGroups();
        assertEquals(2,collGroups.size());

        assertTrue(collGroups.contains(g1));
        assertTrue(collGroups.contains(g2));

        //save persons --------------------------------------------
        personTestHelper1.setLastName("iglesias");
        personTestHelper1.setFirstName("enrique");
        personTestHelper1.setEmail("email1@provider1.com");
        personTestHelper1.setWebsite("http://www.nope.es");
        personTestHelper1.setBirthdate(Date.valueOf("1829-02-27"));
        personTestHelper1.setPassword("ilikeit");
        personTestHelper1.setGroupId(g1.getId());
        
        personTestHelper2.setLastName("polo");
        personTestHelper2.setFirstName("marco");
        personTestHelper2.setEmail("email2@provider2com");
        personTestHelper2.setWebsite("http://www.whynot.es");
        personTestHelper2.setBirthdate(Date.valueOf("2034-05-02"));
        personTestHelper2.setPassword("123456");
        personTestHelper2.setGroupId(g1.getId());
        
        personTestHelper3.setLastName("de magellan");
        personTestHelper3.setFirstName("fernand");
        personTestHelper3.setEmail("email3@provider3com");
        personTestHelper3.setWebsite("http://www.nope.es");
        personTestHelper3.setBirthdate(Date.valueOf("1829-02-27"));
        personTestHelper3.setPassword("ilikeit");
        personTestHelper3.setGroupId(g1.getId());
        
        personTestHelper4.setLastName("de vinci");
        personTestHelper4.setFirstName("leonard");
        personTestHelper4.setEmail("email5@provider5.com");
        personTestHelper4.setWebsite("http://www.wowart.it");
        personTestHelper4.setBirthdate(Date.valueOf("1324-01-08"));
        personTestHelper4.setPassword("davincicode");
        personTestHelper4.setGroupId(g2.getId());

        dao.savePerson(personTestHelper1);
        dao.savePerson(personTestHelper2);
        dao.savePerson(personTestHelper3);
        dao.savePerson(personTestHelper4);

        //check success creation --------------------------------------------
        Collection<Person> collPersonGr1 = dao.findAllPersons(g1.getId());
        Collection<Person> collPersonGr2 = dao.findAllPersons(g2.getId());
        assertEquals(3,collPersonGr1.size());
        assertEquals(1,collPersonGr2.size());

        assertTrue(collPersonGr1.contains(personTestHelper1));
        assertTrue(collPersonGr1.contains(personTestHelper2));
        assertTrue(collPersonGr1.contains(personTestHelper3));
        assertTrue(collPersonGr2.contains(personTestHelper4));

        //prepare modification ----------------------------------------------
        personTestHelper1.setLastName("nom modifie");
        personTestHelper1.setFirstName("prenom modifié");
        personTestHelper1.setEmail("adresse modifiée");
        personTestHelper1.setWebsite("http://modifié");
        personTestHelper1.setBirthdate(Date.valueOf("1680-01-14"));
        personTestHelper1.setPassword("mot de passe modifié");
        personTestHelper1.setGroupId(g1.getId());
        
        personTestHelper2.setLastName("nom modifie2");
        personTestHelper2.setFirstName("prenom modifié2");
        personTestHelper2.setEmail("adresse modifiée2");
        personTestHelper2.setWebsite("http://modifié2");
        personTestHelper2.setBirthdate(Date.valueOf("1680-01-15"));
        personTestHelper2.setPassword("mot de passe modifié2");
        personTestHelper2.setGroupId(g1.getId());
        
        personTestHelper3.setLastName("nom modifie3");
        personTestHelper3.setFirstName("prenom modifié3");
        personTestHelper3.setEmail("adresse modifiée3");
        personTestHelper3.setWebsite("http://modifié3");
        personTestHelper3.setBirthdate(Date.valueOf("1680-01-16"));
        personTestHelper3.setPassword("mot de passe modifié3");
        personTestHelper3.setGroupId(g1.getId());
        
        personTestHelper4.setLastName("nom modifie4");
        personTestHelper4.setFirstName("prenom modifié4");
        personTestHelper4.setEmail("adresse modifiée4");
        personTestHelper4.setWebsite("http://www.wowart.it");
        personTestHelper4.setBirthdate(Date.valueOf("1680-01-17"));
        personTestHelper4.setPassword("mot de passe modifié4");
        personTestHelper4.setGroupId(g2.getId());

        dao.savePerson(personTestHelper1);  //update
        dao.savePerson(personTestHelper2);  //update
        dao.savePerson(personTestHelper3);  //update
        dao.savePerson(personTestHelper4);  //update

        collPersonGr1 = dao.findAllPersons(g1.getId());
        collPersonGr2 = dao.findAllPersons(g2.getId());

        assertEquals(3,collPersonGr1.size());
        assertEquals(1,collPersonGr2.size());

        assertTrue(collPersonGr1.contains(personTestHelper1));
        assertTrue(collPersonGr1.contains(personTestHelper2));
        assertTrue(collPersonGr1.contains(personTestHelper3));
        assertTrue(collPersonGr2.contains(personTestHelper4));
    }
    
    @Test(expected=DAOException.class)
    public void testInsertTwoPersonsWithSameEmail() throws DAOException {
        //prepare groups --------------------------------------------
        Group g1  = new Group("les verts");
        Group g2  = new Group("les bleus");
        
        try {
            dao.saveGroup(g1);
            dao.saveGroup(g2);
            
            Person p1 = new Person("iglesias", "enrique", "super@yahi.en", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());

            dao.savePerson(p1);
        }
        catch (DAOException e) {
            fail("Unexcepted dao exception at this point");
        }
        Person p2 = new Person("polo", "marco", "super@yahi.en", "http://www.whynot.es", Date.valueOf("2034-05-02"),"123456",g2.getId());
        dao.savePerson(p2);
    }

    @Test
    public void testChangePersonGroup() throws DAOException {
        //prepare groups --------------------------------------------
        Group g1  = new Group("les verts");
        Group g2  = new Group("les bleus");

        dao.saveGroup(g1);
        dao.saveGroup(g2);

        //save persons --------------------------------------------
        Person p1 = new Person("iglesias", "enrique", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        dao.savePerson(p1);

        //check success creation --------------------------------------------
        Collection<Person> collPersonGr1 = dao.findAllPersons(g1.getId());
        Collection<Person> collPersonGr2 = dao.findAllPersons(g2.getId());
        assertEquals(1,collPersonGr1.size());
        assertEquals(0,collPersonGr2.size());

        //change person's group
        p1.setGroupId(g2.getId());
        dao.savePerson(p1);

        //check the change effectiveness
        collPersonGr1 = dao.findAllPersons(g1.getId());
        collPersonGr2 = dao.findAllPersons(g2.getId());
        assertEquals(0,collPersonGr1.size());
        assertEquals(1,collPersonGr2.size());

        //check the validity of the person
        assertTrue(collPersonGr2.contains(p1));
    }

    @Test(expected=DAOException.class)
    public void testInvalidChangePersonGroup() throws DAOException {
        //prepare groups --------------------------------------------
        Group g1  = new Group("les verts");

        try {
            dao.saveGroup(g1);
        }
        catch (DAOException e) {
            fail("Unexcepted dao exception at this point");
        }
        
        //save persons --------------------------------------------
        Person p1 = new Person("iglesias", "enrique", "email1@provider1.com", "http://www.nope.es", Date.valueOf("1829-02-27"),"ilikeit",g1.getId());
        
        try {
            dao.savePerson(p1);
            
            //check success creation --------------------------------------------
            Collection<Person> collPersonGr1 = dao.findAllPersons(g1.getId());
            assertEquals(1,collPersonGr1.size());
        }
        catch (DAOException e) {
            fail("Unexcepted dao exception at this point");
        }
        
        //change person's group
        p1.setGroupId(644L);
        dao.savePerson(p1);
    }

    @Test(expected=DAOException.class)
    public void testInvalidPersonZeroId() throws DAOException {
        groupTestHelper.setId(0L);       //invalid id
        groupTestHelper.setName("les verts");
        dao.saveGroup(groupTestHelper);
    }

    @Test(expected=DAOException.class)
    public void testInvalidPersonNegativeId() throws DAOException {
        groupTestHelper.setId(-1L);       //invalid id
        groupTestHelper.setName("les verts");
        dao.saveGroup(groupTestHelper);
    }

    @Test(expected=DAOException.class)
    public void testInvalidGroupZeroId() throws DAOException {
        groupTestHelper.setName("les verts");
        
        try {
            dao.saveGroup(groupTestHelper);
        }
        catch (DAOException e) {
            fail("Unexcepted dao exception at this point");
        }

        personTestHelper1.setLastName("iglesias");
        personTestHelper1.setFirstName("enrique");
        personTestHelper1.setEmail("email1@provider1.com");
        personTestHelper1.setWebsite("http://www.nope.es");
        personTestHelper1.setBirthdate(Date.valueOf("1829-02-27"));
        personTestHelper1.setPassword("ilikeit");
        personTestHelper1.setGroupId(groupTestHelper.getId());
        
        personTestHelper1.setId(0L);
        dao.savePerson(personTestHelper1);
    }

    @Test(expected=DAOException.class)
    public void testInvalidGroupNegativeId() throws DAOException {
        Group g  = new Group("les verts");
        
        try {
            dao.saveGroup(g);
        }
        catch (DAOException e) {
            fail("Unexcepted dao exception at this point");
        }

        personTestHelper1.setLastName("iglesias");
        personTestHelper1.setFirstName("enrique");
        personTestHelper1.setEmail("email1@provider1.com");
        personTestHelper1.setWebsite("http://www.nope.es");
        personTestHelper1.setBirthdate(Date.valueOf("1829-02-27"));
        personTestHelper1.setPassword("ilikeit");
        personTestHelper1.setGroupId(groupTestHelper.getId());
        
        personTestHelper1.setId(-1L);
        dao.savePerson(personTestHelper1);
    }

}
