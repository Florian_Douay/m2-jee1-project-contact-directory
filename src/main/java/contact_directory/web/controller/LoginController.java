package contact_directory.web.controller;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import contact_directory.business.LoginManagerPlainTextPassword;
import contact_directory.dao.Person;
import contact_directory.web.bean.LoginBean;
import contact_directory.web.bean.UserInSession;
import contact_directory.web.validators.LoginValidator;

@Controller
public class LoginController {

    @Autowired
    private LoginManagerPlainTextPassword loginManager;

    @Autowired
    UserInSession userInSession;
    
    @Autowired
    LoginValidator validator;
    
    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginPage() {
        logger.info("GET login page");
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute LoginBean lb, BindingResult result) {
        logger.info("POST login");

        validator.validate(lb, result);
        if (result.hasErrors()) {
            return new ModelAndView("login");
        }
        
        Person person = loginManager.login(lb);
        boolean loginHasSucceeded = person != null;
        
        ModelAndView response = new ModelAndView();
        if(loginHasSucceeded) {
            response.setViewName("redirect:/contact_directory/search");
            userInSession.setId(person.getId());
            userInSession.setGroupId((person.getGroupId()));
        } else {
            // TODO set error message
            response.setViewName("login");
            response.setStatus(HttpStatus.FORBIDDEN);
            response.addObject("loginFailed", true);
        }
        return response;
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ModelAndView logout(HttpSession session) {
        logger.info("POST logout");
        session.invalidate();
        
        ModelAndView response = new ModelAndView();
        response.setViewName("redirect:/contact_directory/search");
        return response;
    }
    
    @ModelAttribute
    public LoginBean createLoginBean(@RequestParam(value = "lastname", required = false) String email,
                                     @RequestParam(value = "firstname", required = false) String password) {
        LoginBean bean = new LoginBean();
        bean.setEmail(email);
        bean.setPassword(password);
        logger.info("new login bean = " + bean);
        return bean;
    }
}
