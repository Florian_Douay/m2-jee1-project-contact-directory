package contact_directory.business;

import java.util.Collection;

import contact_directory.dao.Group;
import contact_directory.dao.exception.DAOException;

public interface GroupManager {

    Collection<Group> findAllGroups() throws DAOException;

    Collection<Group> searchForGroups(String keyword) throws DAOException;

    Group findGroup(long id) throws DAOException;

    void saveGroup(Group g) throws DAOException;
    
}