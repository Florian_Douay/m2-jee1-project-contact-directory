package contact_directory.populator.exception;

@SuppressWarnings("serial")
public class DatabasePopulatorException extends Exception{

    String message;

    @Override
    public String getMessage() {
        return message;
    }

    public DatabasePopulatorException(String message) {
        super(message);
    }

    public DatabasePopulatorException(String message, Throwable cause) {
        super(message, cause);
    }

}
