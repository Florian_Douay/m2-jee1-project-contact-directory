package contact_directory.business;

import java.util.Collection;

import contact_directory.dao.Person;
import contact_directory.dao.PersonColumns;
import contact_directory.dao.exception.DAOException;

public interface PersonManager {

    Collection<Person> findAllPersons(long groupId) throws DAOException;

    Collection<Person> searchForPersons(String keyword, PersonColumns field) throws DAOException;

    Person findPerson(long id) throws DAOException;

    void savePerson(Person p) throws DAOException;

}
