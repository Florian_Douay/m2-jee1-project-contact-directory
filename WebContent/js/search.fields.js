window.onload = function() {
    updateType();
    
    var typeSelect = document.getElementById('type');
    var selectedOption = typeSelect.options[typeSelect.selectedIndex];
    
    if (selectedOption.value === "group") {
        addGroupRowHandlers();  //from list.tr.js
    } 
    else if (selectedOption.value === "person") {
        addPersonRowHandlers(); //from list.tr.js
    }
};

function updateType() {
    var typeSelect = document.getElementById('type');
    var selectedOption = typeSelect.options[typeSelect.selectedIndex];

    var groupCriteriaContainer = document.getElementById('groupCriteriaContainer');
    var personCriteriaContainer = document.getElementById('personCriteriaContainer');
    var groupCriteriaLabel = document.getElementById('groupCriteriaLabel');
    var personCriteriaLabel = document.getElementById('personCriteriaLabel');

    if (selectedOption.value === "group") {
        groupCriteriaContainer.className = "";
        groupCriteriaLabel.className = "";
        personCriteriaContainer.className = "displayNone";
        personCriteriaLabel.className = "sr-only";  //don't display label
    } else if (selectedOption.value === "person") {
        groupCriteriaContainer.className = "displayNone";
        groupCriteriaLabel.className = "sr-only";   //don't display label
        personCriteriaContainer.className = "";
        personCriteriaLabel.className = "";
    } else {
        groupCriteriaContainer.className = "displayNone";
        personCriteriaContainer.className = "displayNone";
        groupCriteriaLabel.className = "sr-only";   //don't display label
        personCriteriaLabel.className = "sr-only";  //don't display label
    }
}
