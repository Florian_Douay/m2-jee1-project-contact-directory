<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/fragments/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<cus:header pageTitle="Login">
    <!--Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/contact.directory.css" media="screen">
</cus:header>

<body>
<main class="container-fluid">

    <nav>
        <%@ include file="/WEB-INF/fragments/menu.jsp"%>
    </nav>

    <section>
    <h2 class="text-center">Login</h2>
    <form:form method="POST" commandName="loginBean">
        <div class="form-group row">
            <div class="col-md-6 col-md-offset-3">
                <label for="email" class="col-form-label">Email</label>
                <form:input path="email" class="form-control"/>
                <span class="help-block error"><form:errors path="email"/></span>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6 col-md-offset-3">
                <label for="password" class="col-form-label">Password</label>
                <form:password path="password" class="form-control"/>
                <span class="help-block error"><form:errors path="password"/></span>
            </div>
        </div>
        <c:if test="${loginFailed == true}">
            <div class="form-group row center-block">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p class="no-result-block">
                       <c:out value="Login failed" />
                    </p>
                </div>
            </div>
        </c:if>
        <div class="form-group row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <input type="submit" class="btn btn-default" />
            </div>
        </div>
    </form:form>
    </section>
</main>
</body>
</html>
