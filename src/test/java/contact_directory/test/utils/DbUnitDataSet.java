package contact_directory.test.utils;

public enum DbUnitDataSet {
    EMPTY_DB("tests/db_unit_empty_dataset.xml"),
    ONE_USER_ONE_GROUP("tests/db_unit_dataset_one_user_one_group.xml");
    
    private String filePath;

    private DbUnitDataSet(String filePath) {
        this.filePath = filePath;
    }
    
    public String getFilePath() {
        return filePath;
    }
}
