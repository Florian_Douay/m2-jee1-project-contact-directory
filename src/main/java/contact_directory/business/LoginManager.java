package contact_directory.business;

import contact_directory.dao.Person;
import contact_directory.web.bean.LoginBean;

public interface LoginManager {
    public Person login(LoginBean loginBean);
}
