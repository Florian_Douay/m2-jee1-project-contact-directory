package contact_directory.populator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.github.javafaker.Faker;

import contact_directory.dao.Group;
import contact_directory.dao.Person;
import contact_directory.dao.PersonDaoJdbc;
import contact_directory.dao.exception.DAOException;
import contact_directory.populator.exception.DatabasePopulatorException;

@Service
public class DatabasePopulator {

    //MYSQL specific
    private static final int CONSTRAINT_VIOLATION_EXCEPTION = 1062;

    @Autowired
    @Qualifier("personDaoJdbc")
    private PersonDaoJdbc personDao;

    private Faker faker = new Faker();
    private Random random = new Random();
    private Date beginBirthDate;
    private Date endBirthDate;
    private final Log logger = LogFactory.getLog(getClass());
    
    public DatabasePopulator() throws DatabasePopulatorException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try { beginBirthDate = sdf.parse("02/01/1970"); }
            catch (ParseException e) { throw new DatabasePopulatorException(e.getMessage(),e.getCause()); }
            endBirthDate = new Date();
    }

    public PersonDaoJdbc getPersonDao() {
        return personDao;
    }

    public void setPersonDao(PersonDaoJdbc personDao) {
        this.personDao = personDao;
    }

    public void populate(int numberOfGroups, int numberOfPersons, int minPersonsPerGroup, int maxPersonsPerGroup) throws DatabasePopulatorException, DAOException {
        //usual checks
        if(numberOfGroups <= 0 ||       //no need to call this function with zero group inserted
           numberOfPersons < 0 ||       //negative values are forbidden
           minPersonsPerGroup < 0 ||    //negative values are forbidden
           maxPersonsPerGroup < minPersonsPerGroup) {   //max must be superior or equal than min
            throw new DatabasePopulatorException("Invalid arguments");
        }
        
        //maximum persons to fill groups
        if(numberOfGroups*maxPersonsPerGroup < numberOfPersons) {
            throw new DatabasePopulatorException("Can't add all persons in groups. Not enough groups or maxPersonsPerGroup too low.");
        }
        
        //minimum persons needed to fill groups
        if(numberOfGroups*minPersonsPerGroup > numberOfPersons) {
            throw new DatabasePopulatorException("Can't add all groups. Not enough persons or minPersonsPerGroup too high.");
        }
        
        int[] randomPersonsNumberPerGroup = generateRandomPersonsNumberPerGroup(numberOfGroups, numberOfPersons, minPersonsPerGroup, maxPersonsPerGroup);
        for(int i=0 ; i<numberOfGroups ; ++i) {
            logger.info("Populating group " + (i+1) + "/" + numberOfGroups + " with " + randomPersonsNumberPerGroup[i] + " persons");
            createGroupAndAddPersonsInGroup(randomPersonsNumberPerGroup[i]);
        }
    }
    
    private int[] generateRandomPersonsNumberPerGroup(int numberOfGroups, int numberOfPersons, int minPersonsPerGroup, int maxPersonsPerGroup) throws DatabasePopulatorException {
        int[] randomPersonsNumberPerGroup;
        //generate one random number between minPersonsPerGroup and maxPersonsPerGroup for each group to create.
        //these numbers will be candidate to be numbers of persons in each group
        try { 
            randomPersonsNumberPerGroup = random.ints(numberOfGroups,minPersonsPerGroup,maxPersonsPerGroup+1).toArray();
            }
        catch (IllegalArgumentException e) { 
            throw new DatabasePopulatorException(e.getMessage(),e.getCause());
        }
        
        int remainingPersons = numberOfPersons;
        int remainingGroups = numberOfGroups;
        
        //the following code is meant to ensure that the proper number of persons and groups are inserted, considering min and max persons per group.
        //the strategy is to calculate the number of possible future persons insertions (with remaining groups to proceed) and to compare it with the remaining number of persons to insert
        //if the randomness is unlucky, we adjust the generated number to match conditions
        for(int i=0 ; i<numberOfGroups ; ++i) {
            
            if(remainingPersons <= maxPersonsPerGroup) {
                randomPersonsNumberPerGroup[i] = remainingPersons;
                remainingPersons=0;
                continue;
            }
            
            //represents the maximum persons we will be able to insert after this group processing.
            int remainingPotential = (remainingGroups-1)*maxPersonsPerGroup;
            
            int generated = randomPersonsNumberPerGroup[i];
            //represents the number of persons we should add to the generated number, in order to be able to put everybody in group AND keeping randomization.
            //without this we might have last few groups full
            int correction = (remainingPersons - generated) - remainingPotential;
            
            if(correction > 0) {
                randomPersonsNumberPerGroup[i] = Math.min(generated+correction,maxPersonsPerGroup);
            }
            
            remainingGroups--;
            remainingPersons -= randomPersonsNumberPerGroup[i];
        }
        return randomPersonsNumberPerGroup;
    }

    private void createGroupAndAddPersonsInGroup(int nbToInsert) throws DAOException {
        Group group = new Group(faker.team().name());
        personDao.saveGroup(group);
        for(int j=0 ; j<nbToInsert ; ++j) {
            insertPersonInGroup(group);
        }
    }

    private void insertPersonInGroup(Group group) throws DAOException {
        
        String lastName = faker.name().lastName();
        String firstName = faker.name().firstName();
        String generatedEmail = faker.internet().emailAddress();
        String finalEmail = lastName + "." + firstName + generatedEmail.substring(generatedEmail.indexOf('@'));
        String website = "www." + lastName + "-" + firstName + "." + faker.internet().domainSuffix();
        Person p = new Person(lastName,
                              firstName,
                              finalEmail,
                              website,
                              new java.sql.Date(faker.date().between(beginBirthDate, endBirthDate).getTime()),
                              faker.internet().password(5, 18),
                              group.getId());
        try {
            personDao.savePerson(p);
        }
        catch (DAOException e) {
            if(e.getErrorCode() != CONSTRAINT_VIOLATION_EXCEPTION) {
                throw e;
            }
            logger.info("Redundant email encountered in database populator. Trying with new random values");
            //We try with new random values
            insertPersonInGroup(group);
        }
    }

}
